window.addEventListener('load', function () {

    var center = [46.05032, 14.46859];
    var trenutna_lokacija = center;
    var map = new L.map('mapa').setView(center, 14);

    var zemljevid = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
    map.addLayer(zemljevid);


    var poligons = [];
    var markers = [];
    var linestrings = [];
    function izrisiBolnice(trenutna_lokacija) {
        poligons.forEach(element => {
            element.remove();
        });
        markers.forEach(element => {
            element.remove();
        });
        linestrings.forEach(element => {
            element.remove();
        });
        poligons = [];
        markers = [];
        linestrings = [];
        for (var x = 0; x<bolnice.length; x++) {
            
//            console.log("trying... " + bolnice[x].properties.name);

            var distance_to_hospital;
            if(bolnice[x].geometry.type == "Polygon") {
                distance_to_hospital = distance(trenutna_lokacija[0], trenutna_lokacija[1], bolnice[x].geometry.coordinates[0][0][0], bolnice[x].geometry.coordinates[0][0][1], 'K');
                
//                console.log(bolnice[x].geometry.coordinates[0][0][0] + " " + bolnice[x].geometry.coordinates[0][0][1]);
            }
            
            else if (bolnice[x].geometry.type == "Point") {
                distance_to_hospital = distance(trenutna_lokacija[0], trenutna_lokacija[1], bolnice[x].geometry.coordinates[0], bolnice[x].geometry.coordinates[1], 'K');
                //  var marker = L.marker(bolnice[x].geometry.coordinates.reverse()).addTo(map);
            }
            else if (bolnice[x].geometry.type == "LineString") {
                distance_to_hospital = distance(trenutna_lokacija[0], trenutna_lokacija[1], bolnice[x].geometry.coordinates[0][0], bolnice[x].geometry.coordinates[0][1], 'K');
            }
            else {
                distance_to_hospital = 999;
            }

            var color;
            if (distance_to_hospital < 5) {
                color = 'green';  
            }
            else {
                color = 'blue';
            }

            if(bolnice[x].geometry.type == "Polygon") {
                var polygon = L.polygon(bolnice[x].geometry.coordinates, {color: color}).addTo(map);
                polygon.bindPopup(('<strong>Ime Ustanove: </strong>' + bolnice[x].properties.name + '<br><strong>Ulica: </strong>' + bolnice[x].properties['addr:street'] + '<br><strong>Hišna številka: </strong>' + bolnice[x].properties['addr:housenumber']).replace(/undefined/g, "Podatek ni na voljo."));
                poligons.push(polygon);
//                console.log(bolnice[x].geometry.coordinates[0][0][0] + " " + bolnice[x].geometry.coordinates[0][0][1]);
            }
            else if (bolnice[x].geometry.type == "Point") {
                if (color == 'blue') {
                    var marker = L.marker(bolnice[x].geometry.coordinates, {icon: blueIcon}).addTo(map);
                    marker.bindPopup(('<strong>Ime Ustanove: </strong>' + bolnice[x].properties.name + '<br><strong>Ulica: </strong>' + bolnice[x].properties['addr:street'] + '<br><strong>Hišna številka: </strong>' + bolnice[x].properties['addr:housenumber']).replace(/undefined/g, "Podatek ni na voljo."));
                    markers.push(marker);
                }
                else {
                    var marker = L.marker(bolnice[x].geometry.coordinates, {icon: greenIcon}).addTo(map);
                    marker.bindPopup(('<strong>Ime Ustanove: </strong>' + bolnice[x].properties.name + '<br><strong>Ulica: </strong>' + bolnice[x].properties['addr:street'] + '<br><strong>Hišna številka: </strong>' + bolnice[x].properties['addr:housenumber']).replace(/undefined/g, "Podatek ni na voljo."));
                    markers.push(marker);
                }
            }     

//        console.log(bolnice[x].properties.name + " " + color);
        }
    }

    map.on('click', function(e) {
      trenutna_lokacija = [e.latlng.lat, e.latlng.lng];
//    console.log(trenutna_lokacija);
      izrisiBolnice(trenutna_lokacija);
    });

    function reverseCoordinates() {
        for (var x = 0; x<bolnice.length; x++) {
            if(bolnice[x].geometry.type == "Polygon") {
                for (let i = 0; i < bolnice[x].geometry.coordinates.length; i++) {
                    for (let j = 0; j < bolnice[x].geometry.coordinates[i].length; j++) {
                        bolnice[x].geometry.coordinates[i][j].reverse();
    //                  console.log(bolnice[x].geometry.coordinates[i][j][0] + " " + bolnice[x].geometry.coordinates[i][j][1]);
                    }
                }
            }
            
            else if (bolnice[x].geometry.type == "Point") {
                bolnice[x].geometry.coordinates.reverse();
            }

            else if (bolnice[x].geometry.type == "LineString") {
                for (let i = 0; i < bolnice[x].geometry.coordinates[i].length; i++) {
                    bolnice[x].geometry.coordinates[i].reverse();
                }
            }
        }
    }

    // Prikazovanje bolnišnic
    var bolnice;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
//            console.log(this.responseText);
            bolnice = JSON.parse(this.responseText).features;
            reverseCoordinates();
            izrisiBolnice(trenutna_lokacija);
        }
    }
    xmlhttp.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
    xmlhttp.send();
});