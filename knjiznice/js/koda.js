var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

// RANDOM VALUES
var names_m = ["Oliver", "Harry", "Jack", "George", "Noah", "Charlie", "Jacob", "Alfie", "Freddie", "Oscar"];
var names_f = ("Olivia,Amelia,Isla,Emily,Ava,Lily,Mia,Sophia,Isabella,Grace").split(",");
var genders = ["MALE", "FEMALE"];
var surnames = ("Smith,Jones,Taylor,Williams,Brown,Davies,Evans,Wilson,Thomas,Roberts").split(",");
var dates = ["1963-02-05", "1974-08-12", "1980-10-18", "1987-04-09", "1996-03-23", "1994-03-13"];
var height_weight_combinations = [[160,50],[174,56],[180,74],[167,50]];

function generirajVzorcnePodatke() {
  var gender = genders[Math.floor(Math.random()*genders.length)];
  var name;
  if (gender == "MALE") name = names_m[Math.floor(Math.random()*names_m.length)];
  else  name = names_f[Math.floor(Math.random()*names_f.length)]; 
  var surname = surnames[Math.floor(Math.random()*surnames.length)];
  var index = Math.floor(Math.random()*height_weight_combinations.length);
  var height = height_weight_combinations[index][0]-2+Math.floor(Math.random()*4);
  var weight = height_weight_combinations[index][1]-2+Math.floor(Math.random()*4);
  var date = dates[Math.floor(Math.random()*dates.length)];
  var oxygen_requirement = 530 + Math.floor(Math.random()*45);

  var returnInformation = {
    gender: gender,
    name: name,
    surname: surname,
    height: height,
    weight: weight,
    dateOfBirth: date,
    oxygen: oxygen_requirement
  }

//  console.log(returnInformation);
  return returnInformation;
}

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}

function izbrisemoNotifications(callback) {
  $("#notifications").html("");
  callback();
}

function izpisiNotifications(sporocilo) {
  var insert = ("<div class='alert alert-info' role='alert'>" + sporocilo + "</div>");
  //console.log(insert);
  $("#notifications").append(insert);
}
/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */

function izpisiPodatkeGumb() {
  generirajPodatkeGumb(function(sporocila) {
    izpisiNotifications(sporocila);
  });
}

function generirajPodatkeGumb(callback) {
  setNotificationType("Generated Astronauts:");
  izbrisemoNotifications(function () {
    for (var i = 0; i < 3; i++) {
      generirajPodatke(generirajVzorcnePodatke(), function(EHRid) {
        izpisiNotifications(EHRid);
      });
    }
  });
}

function setNotificationType(type) {
  $("#notification_type").html("<b>" + type + "</b>");
}

var ranks = ["Officer", "Engineer", "Commander", "Biologist", "IT", "Physicist"]

function setDropdownAstronauts(dropdownAstronauts) {
  generirajPodatke(generirajVzorcnePodatke(), function(astronaut1) {
    generirajPodatke(generirajVzorcnePodatke(), function(astronaut2) {
      generirajPodatke(generirajVzorcnePodatke(), function(astronaut3) {
        vrniPodatkeEHRid(astronaut1, function(astronaut1_info) {
          vrniPodatkeEHRid(astronaut2, function(astronaut2_info) {
            vrniPodatkeEHRid(astronaut3, function(astronaut3_info) {
              dropdownAstronauts.setData([
                {text: (astronaut1_info.parties[0].firstNames + " " + astronaut1_info.parties[0].lastNames + " (" + ranks[Math.floor(Math.random()*ranks.length)] + ")"), value: astronaut1},
                {text: (astronaut2_info.parties[0].firstNames + " " + astronaut2_info.parties[0].lastNames + " (" + ranks[Math.floor(Math.random()*ranks.length)] + ")"), value: astronaut2},
                {text: (astronaut3_info.parties[0].firstNames + " " + astronaut3_info.parties[0].lastNames + " (" + ranks[Math.floor(Math.random()*ranks.length)] + ")"), value: astronaut3}
              ])
            });
          });
        });
      });
    });
  });
}

function generirajPodatke(podatki, callback) {
//  console.log("try");
  $.ajaxSetup({
    headers: {
      "Authorization": getAuthorization()
    }
  });
  $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    success: function(data) {
      var ehrId = data.ehrId;
      var partyData = {
        gender: podatki.gender,
        firstNames: podatki.name,
        lastNames: podatki.surname,
        dateOfBirth: podatki.dateOfBirth,
        additionalInfo: {
          "ehrId": ehrId,
          "weight": podatki.weight,
          "height": podatki.height,
          "oxygen_intake": podatki.oxygen 
        }
      };
//      console.log(JSON.stringify(partyData));
      $.ajax({
        url: baseUrl + "/demographics/party",
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(partyData),
        success: function(party) {
          if (party.action == 'CREATE') {
            console.log("Pacient ustvarjen.");
            console.log(ehrId);
//            vrniPodatkeEHRid(ehrId, function() {});
            callback(ehrId);
            return ehrId;
          }
        },
        error: function(err) {
          console.log(JSON.parse(err.responseText));
        }
      });
    }
  });
}

function vrniPodatkeEHRid(ehrId, callback) {
  $.ajaxSetup({
    headers: {
        "Authorization": getAuthorization()
    }
  });
  var searchData = [
      {key: "ehrId", value: ehrId}
  ];
  $.ajax({
      url: baseUrl + "/demographics/party/query",
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(searchData),
      success: function (res) {
        //console.log(res);
        callback(res);
      },
      error: function(err) {
        console.log("Podatke o pacientu ni bilo mogoce dobiti: " + err);
      }
  });
}

function showValueOxygen(oxygenIntake) {
  $("#CustomOxygenValue").text(oxygenIntake + " L/day");
}

window.addEventListener("load", function() {

  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });

  var dropdownAstronauts = new SlimSelect({
    select: '#AstronautChoice'
  });

  setDropdownAstronauts(dropdownAstronauts);

  window.chartColors = {
    red: 'rgb(255, 99, 132)',
    orange: 'rgb(255, 159, 64)',
    yellow: 'rgb(255, 205, 86)',
    green: 'rgb(75, 192, 192)',
    blue: 'rgb(54, 162, 235)',
    purple: 'rgb(153, 102, 255)',
    grey: 'rgb(201, 203, 207)'
  };

  window.randomScalingFactor = function() {
		return (1+Math.floor(Math.random()*100));
	};

  var calorie_config = {
    type: 'line',
    data: {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [{
        label: 'Calorie Intake',
        backgroundColor: window.chartColors.red,
        borderColor: window.chartColors.red,
        data: [],
        fill: false,
      }]
    },
    options: {
      responsive: true,
      title: {
        display: true,
        text: ''
      },
      tooltips: {
        mode: 'index',
        intersect: false,
      },
      hover: {
        mode: 'nearest',
        intersect: true
      },
      scales: {
        xAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Days'
          }
        }],
        yAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'calories per day'
          }
        }]
      }
    }
  };

  var oxygen_config = {
    type: 'line',
    data: {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [{
        label: 'Oxygen Consumption',
        fill: false,
        backgroundColor: window.chartColors.blue,
        borderColor: window.chartColors.blue,
        data: [],
      }]
    },
    options: {
      responsive: true,
      tooltips: {
        mode: 'index',
        intersect: false,
      },
      hover: {
        mode: 'nearest',
        intersect: true
      },
      scales: {
        xAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Days'
          }
        }],
        yAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'liters of oxygen per day'
          }
        }]
      }
    }
  };

  var ctx_c = $('#CalorieIntake');

  var LineChartCalorie = new Chart(ctx_c, calorie_config);

  var ctx_o = $('#OxygenConsumption');

  var LineChartOxygen = new Chart(ctx_o, oxygen_config);

  var spaceship_speed = 30;

  function simulate(planet) {
    $("#distance").html(planets[planet]);
    console.log("SIMULATE starting");

    var length = Math.floor(planets[planet]/spaceship_speed);
    if (length==1){length=2;}
    $("#journeylength").html(length + " days");
    // console.log(length);

    var astronautInformation = getAstronautInformation();

    var a = moment(astronautInformation.dateOfBirth);
    var b = moment();
    var age = Math.abs(a.diff(b, 'years'));

    var caloriesperday = Math.floor(10 * astronautInformation.weight + 6.25 * astronautInformation.height - 5*age + 50);
    //$("#averageCalories").html(caloriesperday + "cal");

    //simulate calorie intake per day
    var total_calorie_difference = 0;
    var total_calories = 0;
    var calories = [];
    var days = [];
    for (var i = 0; i < length; i++) {
      days[i] = i+1;
      calories[i] = caloriesperday - 250 + Math.floor(Math.random()*601);
      total_calories += calories[i];
      total_calorie_difference += calories[i]-caloriesperday;
    }

    var weight_difference = total_calorie_difference/3500/2;
    weight_difference = Math.round(weight_difference * 100) / 100

    $("#weightgain").html(weight_difference + "kg");

    var costoffood = total_calories/1200;
    costoffood = Math.round(costoffood * 100) / 100;

    $("#costfood").html(costoffood + " EUR");

    var oxygens = [];
    var total_oxygen = 0;

    for (var i = 0; i < length; i++) {
      oxygens[i] = astronautInformation.oxygenintake - 10 + Math.floor(Math.random()*21);
      total_oxygen += oxygens[i];
    }

    var costofoxygen = total_oxygen/100;
    costofoxygen = Math.round(costofoxygen * 100) / 100;

    $("#costoxygen").html(costofoxygen + " EUR");
    
    calorie_config.data.datasets[0].data = calories;
    calorie_config.data.labels = days;
    oxygen_config.data.labels = days;
    oxygen_config.data.datasets[0].data = oxygens;
    LineChartCalorie.update();
    LineChartOxygen.update();


  
  }

  $("#simulateMERCURY").click(function() {
    simulate('MERCURY');
  });
  $("#simulateVENUS").click(function() {
    simulate('VENUS');
  });
  $("#simulateMARS").click(function() {
    simulate('MARS');
  });
  $("#simulateJUPITER").click(function() {
    simulate('JUPITER');
  });
  $("#simulateSATURN").click(function() {
    simulate('SATURN');
  });
  $("#simulateURANUS").click(function() {
    simulate('URANUS');
  });
  $("#simulateNEPTUNE").click(function() {
    simulate('NEPTUNE');
  });
  $("#simulatePLUTO").click(function() {
    simulate('PLUTO');
  });

  $("#dropdownAstronautButton").click(function() {
    ImportDataFromDropdown(dropdownAstronauts);
  });
});



function ImportDataFromEHRid() {
  var ehrId = $("#EHRidCustom").val();
  //console.log(ehrId);
  vrniPodatkeEHRid(ehrId, function(res) {
    console.log(res);
    var info = {
      name: "",
      surname: "",
      dateOfBirth: "",
      ehrId: null,
      weight: null,
      height: null,
      oxygenintake: null
    };

    info.name = res.parties[0].firstNames;
    info.surname = res.parties[0].lastNames;
    info.dateOfBirth = res.parties[0].dateOfBirth;
    info.ehrId = res.parties[0].additionalInfo.ehrId;
    info.weight = res.parties[0].additionalInfo.weight;
    info.height = res.parties[0].additionalInfo.height;
    info.oxygenintake = res.parties[0].additionalInfo.oxygen_intake;

    fillInformation(info, function(state) {
      if (state) {
        console.log("Filling was succeseful");
        valid_astronaut = true;
        startApp();
      }
      else {
        console.log("There has been an error trying to fill astronaut information.");
        console.log(info);
      }
    });
  });
}

function ImportDataFromDropdown(dropdownAstronauts) {
  var ehrId = dropdownAstronauts.selected();

  vrniPodatkeEHRid(ehrId, function(res) {
    console.log(res);
    var info = {
      name: "",
      surname: "",
      dateOfBirth: "",
      ehrId: null,
      weight: null,
      height: null,
      oxygenintake: null
    };

    info.name = res.parties[0].firstNames;
    info.surname = res.parties[0].lastNames;
    info.dateOfBirth = res.parties[0].dateOfBirth;
    info.ehrId = res.parties[0].additionalInfo.ehrId;
    info.weight = res.parties[0].additionalInfo.weight;
    info.height = res.parties[0].additionalInfo.height;
    info.oxygenintake = res.parties[0].additionalInfo.oxygen_intake;

    fillInformation(info, function(state) {
      if (state) {
        console.log("Filling was succeseful");
        valid_astronaut = true;
        startApp();
      }
      else {
        console.log("There has been an error trying to fill in astronaut information.");
        console.log(info);
      }
    });
  });
}

function UploadNewAstronaut(callback) {
  
  var name = $("#CustomFirstName").val();
  var surname = $("#CustomLastName").val();
  var dateOfBirth = $("#CustomDateOfBirth").val();
  var weight = $("#CustomWeight").val();
  var height = $("#CustomHeight").val();
  var oxygenintake = $("#CustomOxygen").val();

  var podatki = {
    name: name,
    surname: surname,
    dateOfBirth: dateOfBirth,
    weight: weight,
    height: height,
    oxygen: oxygenintake
  };

  var valid_information = true;

  if (name.length == 0) {
    valid_information = false;
    $("#CustomFirstNameAlert").html("Please enter a name.");
  }
  else {$("#CustomFirstNameAlert").html("");}
  if (surname.length == 0) {
    valid_information = false;
    $("#CustomLastNameAlert").html("Please enter a last name.");
  }else {$("#CustomLastNameAlert").html("");}
  if (dateOfBirth.length == 0) {
    valid_information = false;
    $("#CustomDateOfBirthAlert").html("Please enter a date of birth.");
  }else {$("#CustomDateOfBirthAlert").html("");}
  if (weight.length == 0) {
    valid_information = false;
    $("#CustomWeightAlert").html("Please enter weight");
  }else {$("#CustomWeightAlert").html("");}
  if (height.length == 0) {
    valid_information = false;
    $("#CustomHeightAlert").html("Please enter height.");
  }else {$("#CustomHeightAlert").html("");}

  if (valid_information) {
    generirajPodatke(podatki, function(ehrId) {
      izbrisemoNotifications(function() {
        setNotificationType("New astronaut added to the database:");
        izpisiNotifications(ehrId);
        if (callback) callback(ehrId);
      });
    });
  }

}

function UploadNewAstronautAndUse() {
  UploadNewAstronaut(function(ehrId) {
    vrniPodatkeEHRid(ehrId, function(res) {
      console.log(res);
      var info = {
        name: "",
        surname: "",
        dateOfBirth: "",
        ehrId: null,
        weight: null,
        height: null,
        oxygenintake: null
      };
  
      info.name = res.parties[0].firstNames;
      info.surname = res.parties[0].lastNames;
      info.dateOfBirth = res.parties[0].dateOfBirth;
      info.ehrId = res.parties[0].additionalInfo.ehrId;
      info.weight = res.parties[0].additionalInfo.weight;
      info.height = res.parties[0].additionalInfo.height;
      info.oxygenintake = res.parties[0].additionalInfo.oxygen_intake;
  
      fillInformation(info, function(state) {
        if (state) {
          console.log("Filling was succeseful");
          valid_astronaut = true;
          startApp();
        }
        else {
          console.log("There has been an error trying to fill in astronaut information.");
          console.log(info);
        }
      });
    });
  });
}

// =======================================================
// Use of the application
var planets = JSON.parse('{"MERCURY":91,"VENUS":41,"EARTH":0,"MOON":"0.384*","MARS":227,"JUPITER":778,"SATURN":1433,"URANUS":2872,"NEPTUNE":4495,"PLUTO":5906," ":null}');

var valid_astronaut = false;

var astronautInformation = {
  name: "",
  surname: "",
  dateOfBirth: "",
  ehrId: null,
  weight: null,
  height: null,
  oxygenintake: null
};

function getAstronautInformation() {
  return astronautInformation;
}

function fillInformation(info, callback) {
  // Check if information is valid
  if (info.name.length == 0 ||
      info.surname.length == 0 ||
      info.dateOfBirth.length == 0 ||
      info.ehrId.length == 0 ||
      info.weight.length == 0 ||
      info.height.length == 0 ||
      info.oxygenintake.length == 0) {
    callback(false);
  }

  else {
    astronautInformation.name = info.name;
    astronautInformation.surname = info.surname;
    astronautInformation.dateOfBirth = info.dateOfBirth;
    astronautInformation.ehrId = info.ehrId;
    astronautInformation.weight = info.weight;
    astronautInformation.height = info.height;
    astronautInformation.oxygenintake = info.oxygenintake;
    callback(true);
  }
}



function startApp() {
  console.log("App is starting with");
  console.log(astronautInformation);
  
  $("#name").html(astronautInformation.name);
  $("#surname").html(astronautInformation.surname);
  $("#date").html(astronautInformation.dateOfBirth);
  $("#height").html(astronautInformation.height);
  $("#weight").html(astronautInformation.weight);
  $("#oxygen").html(astronautInformation.oxygenintake);

  var a = moment(astronautInformation.dateOfBirth);
  var b = moment();
  var age = Math.abs(a.diff(b, 'years'));

  var caloriesperday = 10 * astronautInformation.weight + 6.25 * astronautInformation.height - 5*age + 50;
  $("#averageCalories").html(Math.floor(caloriesperday) + "cal");

  // Remove overlay
  $("#disableOverlay").hide();
}


// =======================================================
/*
*
ImportDataFromEHRid
--Check if valid information
ImportDataFromDropdown
UploadNewAstronaut
--Check if valid information
UploadNewAstronautAndUse
--Check if valid information
*/
// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
